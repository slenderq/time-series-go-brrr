# time-series-go-brrr

A simple python program that multiplex's multiple time series model on the provided command line dataset. Then outputs a pdf of results 


## usage

```
todo: probably some python interface here? 
```

### Cli
tsgbrrr my-tabular-data.csv --target COL --timecol COL 

# tsgbrrr my-tabular-data.csv # assumes target is last column



## models

### Naive

#### 'Yesterday' prediction 
We just predict today will be like the same time yesterday. and it might be better

#### ARIMA
Back to basics

### Classical

#### Random Forest / Other time series relevent sklearn models
from scilearn? 

#### XGBoost and / or CatBoost

### Deep learning
LSTM 

## rational / philosophy
### Test train split
There is a autocorrelation based split eventually but intially a chunkin
### 
This tool is to 

### Using Nix to go brrr
Because nix can assemble all of the these different packages into some sane versioned mess, it will be lightweight and fully featured.
this will sharpen my nixos but also it never a bad thing brining more of the data scince world into nixos. They give me everything I want in an industrial setting.


## flake tools? 
# https://github.com/jqnatividad/qsv
probably a bunch of my regular data science tools 

also textfile based juypter notebooks and notebook example

## todo
nix flake move over to this repo and slowly start adding packages.

I should probably find all of the packages in one go.


## algo?
https://machinelearningmastery.com/branching-out-exploring-tree-based-models-for-regression/
